﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkId=232509
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var local = WinJS.Application.local;

    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }

    // Установить обработчики клавиатуры
    function nocopy(event) {
        var event = event || window.event;
        if (event.preventDefault) { event.preventDefault(); }
        else { event.returnValue = false; }
        return false;
    }

    function goToPageStory() {
        document.location.href = "/html/openStoryPage.html";
    }
 
    var storyArr = ["empty",
        "Мужчина ночью долго ворочался в кровати и никак не мог заснуть... Потом он взял телефон, набрал чей-то номер, прослушав несколько длинных гудков - положил трубку и спокойно заснул. Вопрос: почему он до этого не мог заснуть?",
        "Шёл человек. У него на пальце было большое золотое кольцо. На человека напали разбойники. Они хотели снять у него это кольцо, но оно не снималось, так что они отрезали палец, но мужик не закричал. Вопрос: Почему человек не закричал?",
        "Он ей что-то сказал, и она умерла. Вопрос: Что произошло?",
        "Он был неизвестен, потом нарушил инструкцию, погиб и прославился. Вопрос: Кто он?",
        "Молодой человек сидит в троллейбусе рядом с дамой. Входит её подруга и дамы начинают разговаривать. Молодой человек встаёт и предлагает вошедшей даме сесть. Она обзывает его нахалом. Вопрос: почему?",
        "Известный иллюзионист и гипнотизёр Иванов абсолютно точно предсказывает счёт любого баскетбольного матча до того, как он начнётся. Вопрос: Как это ему удаётся?",
        "Девушка расказывает: 'Однажды вечером мой дядюшка читал интересную книгу. Тётушка по рассеянности выключила свет и в комнате стало темно. Книга была такая интересная, что дядюшка не стал отрываться, чтобы включить свет, и дочитал книгу до конца в темноте'. Вопрос: Как ему это удалось?",
        "Как такое может быть: 19 + 11 = 6? Ответ: Это может быть, если к 19 часам прибавить 11 часов: получится 6 часов утра!",
        "Лежит труп посередине поля. Возле него лежит рюкзак. Следов вокруг нету. Вопрос: Что случилось?",
        "Был один коллекционер монет, ему сообщили, что в Риме найдена монета с надписью: 53 год до Р.Х. 3 флорины на что коллекционер монет сказал: 'монета, конечно, поддельная!'",
        "В корзине лежит 5 яблок. Как разделить яблоки между пятью людьми так, чтобы каждый получил по яблоку, но одно яблоко осталось в корзине? Ответ: Надо раздать четверым по яблоку, а одному отдать корзину с пятым яблоком.",
        "У трех людей есть брат Иван, а у Ивана братьев нет. Вопрос: Как такое возможно?",
        "На столе сидело три мухи. Я взял тапок и прибил им одну муху. Вопрос: Сколько мух осталось на столе?",
        "Сколько земли содержит яма размером 2х2х2 метра? Ответ: В яме нет земли, так как яма - это наоборот отсутствие земли.",
        "Кошке к хвосту привязали консервную банку. С какой скоростью должна бежать кошка, чтобы не слышать грохота банки, гремящей об асфальт? Ответ: кошка просто сидит на месте, тогда она и не услышит гремящей банки.",
        "Видели ли вы, как ловят рыбу сетью? А можно ли рыболовной сетью вытащить из реки воду, причем так, чтобы вся сеть была полной? Ответ: Можно, если сетью зачерпнуть льда!",
        "В одном месте сидела девушка, затем она поднялась со своего места и ушла, вы бы хотели сесть на ее место, но не можете этого сделать. Вопрос: Где она сидела?",
        "Можно ли мужчине брать в жены сестру своей вдовы? Ответ: Довольно сложно сочетаться браком, если ты мертв.",
        "Зачем утенок переходит через дорогу? Ответ: Затем, что обходить ее вокруг было бы слишком долго.",
        "Как-то раз с утра один человек, отправляясь на работу, заметил, что у его машины одно колесо совсем спущено. Несмотря на это, он сел в машину и доехал до работы. Вечером, после работы, человек съездил в магазин, купил продуктов и без проблем вернулся домой, так и не заехав на шиномонтаж и ни разу не подкачав колесо насосом. Вопрос: Как, а главное, зачем он ездил со спущенным колесом так долго?",
        "Человек получает посылку, в которой лежит отрубленная рука и записка «это не та рука». Вопрос: Что произошло?",
        "Получив письмо мужа, женщина поняла, что он умер. Вопрос: Как так?",
        "Голый мужчина дрожит от холода, в недоумении, стоя над еще теплым трупом. Вопрсо: Что произошло?",
        "Женщина не знала, как избавиться от засидевшихся гостей, но её спас телефонный звонок. Вопрос: Как ?",
        "Два водителя получили серьезные травмы, но на машинах не было следов столкновения. Объясните. Ответ: Машины двигались на встречу друг другу в сильном тумане. Водители одновременно высунули головы из окон, чтобы посмотреть, что впереди. И столкнулись головами.",
        "Человек очень долго не выходил из дома и к нему никто не приходил. В один прекрасный вечер он выключил свет, закрыл дверь и ушел. В эту ночь по его вине погибло 200 человек. Вопрос: Что произошло?",
        "Человек спускался вниз по лестнице и вдруг понял, что в эту минуту его жена умерла. Вопрос: Как это возможно?",
        "Мужчина заходит в бар и просит стакан воды, бармен внезапно достает ружье и направляет на мужчину. Мужчина говорит «спасибо» и уходит. Вопрос: Что произошло?",
        "Человек пошел на вечеринку и там выпил немного пунша. Затем он покинул вечеринку самым первым. Все остальные люди, которые пили пунш после него, умерли от отравления. Вопрос: Почему этот человек остался жив?",
        "Дженни отметила свое день рождения. А через два дня её сестра близнец отметит свое. Вопрос: Как это возможно?",
        "Посреди пустыни лежит мёртвый человек, со сгоревшей спичкой в руках. На много километров вокруг ни одной живой души… Вопрсо:  Что произошло с этим человеком? Как он туда попал?",
        "Слепой человек заходит в ресторан и заказывает мясо альбатроса. Поужинав, он выходит из ресторана, достает пистолет и стреляет себе в голову. Вопрос: Что произошло?",
        "Экзамен в военном училище. Студент вытянул билет и сел готовиться. Через минуту он подошел к преподавателю, молча, протянул зачетку и преподаватель, не говоря ни слова, ставит ему 5. Вопрсо: Почему?",
        "Одна женщина получила огромное наследство от бабушки. Его доставил курьер в конверте. Но когда женщина вскрыла конверт, там лежала только купюра в 5 долларов. Она в досаде смяла и выбросила конверт с «наследством» в мусорное ведро. На следующий день горничная женщины уволилась, а через месяц женщина узнала, что её бывшая горничная стала миллионершей. Вопрос: В чем секрет?",
        "Одному старику очень мешали дети, которые постоянно играли и кричали у него под окнами. Но горстка мелочи помогла ему решить проблему и добиться тишины. Вопрос: Как?",
        "В комнате за столом сидят люди. Они все мертвы. Перед одним из них лежит незаряженный пистолет и карты. Вопрос: Что с ними произошло?",
        "Птицелов увидел птицу, а чуть позже умер.",
        "Герои пьесы Шекспира 'Сон в летнюю ночь' - эльфы и феи. Актеры обычно играли в пышных средневековых костюмах, чтобы показать, что действие происходит в старину. Но один режиссер решил, что это неправильно. Феи и эльфы - персонажи народных сказок, они не должны быть роскошно одеты. Противоречие: костюмы должны быть пышными, чтобы казаться старинными, и должны быть простонародными. Вопрос: Как быть?",
        "Музыка смолкла, и она умерла.",
        "Посреди океана стоит комфортабельная яхта, а рядом с нею плавает шесть трупов. Ответ: Ответ: Хозяева этой яхты попрыгали в воду с борта, забыв опустить веревочную лестницу. В результате они не смогли подняться обратно на борт и умерли от переохлаждения в воде. Другой вариант: лестница была спущена, но доставала до поверхности воды только тогда, когда на яхте были люди. После того как все оказались за бортом, осадка яхты уменьшилась и лестница оказалась вне пределов досягаемости людей.",
        "Суперробот Джанни (всепроникающий, всеразрушающий и т.п.) дал герою клятву: 'Ни кто иной, как я, убью тебя!'. Джанни обещает герою выполнить одну предсмертную просьбу. Как спастись герою? Ответ: Ответ: П.Энтони 'Никто иной, как я'. Герой просит убить его через 50 лет. Все эти годы Джанн будет его верным телохранителем, спасая его от любой другой смерти.",
        "Американец Чарльз Сембри был разъярен, когда через 20 (!) лет выяснил, что слышит без слухового аппарата лучше, чем с ним. Он отправился с претензиями к врачу, рекомендовавшему ношение аппарата, но врач разъярился не меньше Сембри. Ответ: Ответ: Больной носил аппарат на здоровом ухе.",
        "Жил-был парень Билл. У него была Мери, которую он очень любил. Однажды, когда Билл был в кафе, разразилась сильная гроза. Когда Билл вернулся домой, он увидел распахнутое окно и лежащую среди осколков стекла в луже воды мертвую Мери. Что произошло? Ответ: Ответ: Аквариум с рыбкой Мэри упал и разбился.",
        "Полиция Венесуэллы несколько раз пыталась арестовать известного бандита.Они знали, где он живет. Иногда, получив ордер на его арест, они отправлялись к нему домой. Но как только они входили в дом, он запирался в спальне. Полицейским ничего не оставалось делать, кроме как уйти ни с чем. Почему так получалось? Ответ: Ответ: Его дом стоял на границе Венесуэлы и Колумбии. Хотя входная дверь и полдома были расположены на территории Венесуэлы, спальня была расположена натерритории Колумбии. Венесуэльская полиция не имела юрисдикции в Колумбии,поэтому полицейские не могли его арестовать, пока он находился в спальне.",
        "Дама смотрит из окна поезда Ленинград - Вильнюс на проплывающие поля, леса, луга и, ни к кому конкретно не обращаясь, говорит: 'Интересно, мы еще в Латвии или уже в Литве?'. Сидящий рядом литовец откладывает книгу и ровно минуту смотрит в окно. За окном поля, луга... 'Мы уже в Литве', - уверенно говорит он. Ответ: В Литве все коровы рыжей породы.",
        "Женщина входит в комнату, закрывает дверь на ключ и начинает раздеваться. В это время гаснет свет и раздается свист.",
        "Поженились они по любви. Жили душа в душу, занимались только друг другом. В результате стали миллионерами.",
        "Авария в лифте. Застряли 4 человека. Трое расстроены, один доволен.",
        "На одном из отделений фермы резко ухудшилось качество говядины. Фермер не разобрался сам, пригласил специалистов, но обследование ничего не дало: все нормы выполнялись неукоснительно.",
        "Некто позвонил в полицию и сообщил, чтобы они поинтересовались Джоном К. Прибыв к Джону К. домой, полиция обнаружила его мертвым. Обследовав труп, полиция обнаружила тяжкие телесные повреждения. Но квартира была в полном порядке, никаких следов борьбы и насилия не было обнаружено.",
        "Черное-черное на одной ноге. Что это?",
        "Расследование убийства, совершенного на Луне, производится через день на Земле. Убийца, пробывший на Луне несколько дней, рассчитывает, что никто не сможет доказать, что он был там, так как через скафандр ничто не сможет проникнуть, а скафандр уничтожен. ",
        "Профессор смотрит в замочную скважину и видит: отец сына профессора дерется с сыном отца профессора. Вопрос: Кто с кем дерется?",
        "В газете помещена заметка 'Смерть в горах' с фотографией супружеской пары и соболезнованием мужу по поводу гибели жены. Некто пришел в полицию, после чего муж был арестован по подозрению в убийстве.",
        "Мертвый человек найден у себя дома лежащим в луже из воды и крови.",
        "Две американки разговаривали. Одна зашла в ванную комнату, а через пять минут вышла и убила другую.",
        "Человек, не имеющий заграничного паспорта, в течение одного дня посещает тридцать разных стран. В каждой из них его встречают и провожают, причем каждую страну он покидает по собственной воле.",
        "Человек лежит вблизи большого здания с дыркой в черепе.",
        "Оскальпированная женщина лежит на улице около автомобиля",
        "Человек, толкавший свою машину, остановился возле гостиницы. Как только это произошло, он понял, что обанкротился."
    ];
    var headerStoryArr = ["empty",
        "Бессоница",
        "Человек с кольцом",
        "Слово-убийца",
        "Герой посмертно",
        "Случай в троллейбусе",
        "Провидец",
        "Книга в ночи",
        "19+11=6",
        "Труп посреди поля",
        "Древняя монета",
        "Разделить яблоки",
        "Брат",
        "Мухи на столе",
        "Яма",
        "Банка на хвосте",
        "Сеть, полная воды",
        "Странное место",
        "Свадьба",
        "Утенок на дороге",
        "Спустило колесо",
        "Рука",
        "Письмо от мужа",
        "Двое",
        "Спасительный звонок",
        "Загадочное столкновение",
        "Одиночество",
        "Внезапная смерть",
        "Случай в баре",
        "Смертельный пунш",
        "День рождения",
        "Человек со спичкой",
        "Мясо альбатроса",
        "Пятерка за экзамен",
        "Наследство",
        "Старик и дети",
        "Смертельное застолье",
        "Птицелов",
        "Шекспир ",
        "Музыка",
        "Посреди океана",
        "Суперробот",
        "Американец",
        "Жил-был",
        "Полиция ",
        "Поезд Ленинград - Вильнюс",
        "Свист",
        "Миллионеры",
        "Лифт",
        "Ферма",
        "Некто",
        "Одноногий",
        "Убийство на Луне",
        "Проффесор",
        "Смерть в горах",
        "Мертвый человек",
        "Две американки",
        "Человек без паспорта",
        "Дырка в черепе",
        "Женщина...",
        "Банкрот"
    ];
    var answerArr = ["empty",
        "Ответ: За стеной громко храпел сосед, который потом проснулся от телефонного звонка.",
        "Ответ: У него были золотые зубы и он не хотел их показывать, чтобы их тоже не забрали.",
        "Ответ: Акробат в цирке держал партнёршу зубами.",
        "Ответ: Легенда об Икаре.",
        "Ответ: Дама не помещается в сиденье.",
        "Ответ: Дама не помещается в сиденье.",
        "Ответ: Счёт до начала игры всегда равен 0:0. Предсказать это может кто угодно.",
        "Ответ: Счёт до начала игры всегда равен 0:0. Предсказать это может кто угодно.",
        "Ответ: Книга напечатана шрифтом Брайля (шрифт для незрячих, читается наощупь).",
        "Ответ: Это может быть, если к 19 часам прибавить 11 часов: получится 6 часов утра!",
        "Ответ: Парашют не раскрылся ;(",
        "Ответ: Откуда римляне могли знать, что через 53 года будет Рождество Христово!",
        "Ответ: Надо раздать четверым по яблоку, а одному отдать корзину с пятым яблоком.",
        "Ответ: Это возможно, если эти люди - женщины, или же речь идёт о разных Иванах.",
        "Ответ: Осталась одна муха, которой досталось тапком. Остальные улетели!",
        "Ответ: В яме нет земли, так как яма - это наоборот отсутствие земли.",
        "Ответ: кошка просто сидит на месте, тогда она и не услышит гремящей банки.",
        "Ответ: Можно, если сетью зачерпнуть льда!",
        "Ответ: У вас на коленях. Сесть себе на колени вы не можете.",
        "Ответ: Довольно сложно сочетаться браком, если ты мертв.",
        "Ответ: Затем, что обходить ее вокруг было бы слишком долго.",
        "Ответ: Спустило запасное колесо.",
        "Ответ: Несколько лет назад человек потерпел кораблекрушение и оказался на необитаемом острове с другим бедолагой. Они договорились, что поочередно отрежут каждому левую руку и съедят её на двоих. До этого человека очередь не дошла — их спасли, но по условиям договора он должен был отрезать себе руку по возвращению и выслать в посылке парню, рука которого была съедена. Человек пытался схитрить и выслать чужую руку. Но его обман был раскрыт.",
        "Ответ: Женщина непосредственно причастна к смерти мужа. Она отправила ему письмо, вложив в него отравленные марки, для обратного ответа. Получив ответ, с отравленной маркой на конверте, она поняла, что план сработал.",
        "Ответ: Мужчина очнулся от летаргического сна в морге. Работник морга, увидев «живого мертвеца», умер от инфаркта.",
        "Ответ: Женщина сделала вид, что звонивший сообщил ей о пожаре в доме одного из гостей, но она не расслышала, о чьем доме шла речь.",
        "Ответ: Машины двигались на встречу друг другу в сильном тумане. Водители одновременно высунули головы из окон, чтобы посмотреть, что впереди. И столкнулись головами.",
        "Ответ: Человек был смотрителем маяка. Уходя, он погасил свет на маяке, и корабль потерпел кораблекрушение.",
        "Ответ: Человек находился в больнице. Его жена была там же, подключенная к аппарату искусственного жизнеобеспечения. Когда он спускался по лестнице в больнице пропало электричество и погас свет . Соответственно, аппарат тоже отключился.",
        "Ответ: Человек мучился от икота и зашел в ближайший бар, выпить воды. Бармен понял, в чем его проблема и применил испытанное средство — напугать икающего человека. Способ сработал и мужчина поблагодарил его.",
        "Ответ: Яд был в кубиках для льда. Человек пил пунш самым первым, и лед еще не успел растаять и смешаться с напитком.",
        "Ответ: Дженни родилась 28 февраля, за несколько минут до полуночи. А её сестра уже 1-го марта.Получается, что в високосный год день рождения младшей из близняшек на 2 дня позже.",
        "Ответ: Человек летел вместе со своими друзьями на воздушном шаре. Их воздушный шар начал падать и им пришлось тянуть спичку: кому выпадет сгоревшая спичка, тому прийдется прыгать. этому человеку попалась сгоревшая спичка и он прыгнул и разбился насмерть.",
        "Ответ: Несколько лет назад слепой человек потерпел кораблекрушение и попал с женой и другом на необитаемый остров. Несколько дней они голодали, затем жена и друг ушли на поиски пищи. Друг вернулся один и сказал, что жена, должно быть, заблудилась и вернется позже. Зато, по его словам, ему удалось наловить альбатросов. Жена так и не вернулась, а следующие несколько дней друг кормил слепого парня «мясом альбатроса». Попробовав настоящее мясо альбатроса, человек понял, что ел собственную жену, убитую его другом.",
        "Ответ: Преподаватель, используя азбуку морзе, простучал по столу карандашом сообщение «Кто подойдет первым, сразу получит оценку отлично». Студент расшифровал сообщение и получил заслуженную пятерку.",
        "Ответ: Бабушка женщины любила пошутить — на конверте была наклеена коллекционная марка, стоимостью 3 миллиона долларов.",
        "Ответ: Старик сказал детям, что ему очень нравится, когда они кричат у него под окнами и он готов платить им деньги, при условии, что они будут приходить во двор каждый день и кричать, как можно громче. Через пару дней, он сказал детям, что платить ему больше нечем. Дети убежали играть в другое место, не желая бесплатно веселить старика.",
        "Ответ: Они находились в подводной лодке и они начали тонуть. у них был пистолет с одной пулей. они решили сыграть в карты: кто выиграл, тому пистолет с пулей, и тот может застрелиться, а остальные, умрут мучительной смертью…",
        "Ответ: Птицелов летел на реактивном пассажирском самолете, а птицу, увиденную им, засосало в один из двигателей. В результате самолет разбился.",
        "Ответ: а) часть платья пышная, часть простонародная; б) грубые заплаты на нарядных платьях.",
        "Ответ: Ответ: Она - артистка цирка, канатоходка. Ее коронный номер - ходьба по канату с завязанными глазами. Окончание музыки должно было служить ей сигналом, что путь завершен и можно сделать шаг в сторону. К сожалению, дирижер циркового оркестра (вариант: солистскрипач) неожиданно заболел, а его сменщика забыли предупредить, поэтому он закончил мелодию раньше, чем артистка дошла до конца каната. В результате она упала с высоты и разбилась.",
        "Ответ: Хозяева этой яхты попрыгали в воду с борта, забыв опустить веревочную лестницу. В результате они не смогли подняться обратно на борт и умерли от переохлаждения в воде. Другой вариант: лестница была спущена, но доставала до поверхности воды только тогда, когда на яхте были люди. После того как все оказались за бортом, осадка яхты уменьшилась и лестница оказалась вне пределов досягаемости людей.",
        "Ответ: П.Энтони 'Никто иной, как я'. Герой просит убить его через 50 лет. Все эти годы Джанн будет его верным телохранителем, спасая его от любой другой смерти.",
        "Ответ: Больной носил аппарат на здоровом ухе.",
        "Ответ: Аквариум с рыбкой Мэри упал и разбился.",
        "Ответ: Его дом стоял на границе Венесуэлы и Колумбии. Хотя входная дверь и полдома были расположены на территории Венесуэлы, спальня была расположена натерритории Колумбии. Венесуэльская полиция не имела юрисдикции в Колумбии,поэтому полицейские не могли его арестовать, пока он находился в спальне.",
        "Ответ: В Литве все коровы рыжей породы.",
        "Ответ: Во время киносеанса порвалась пленка.",
        "Ответ: До этого они были миллиардерами.",
        "Ответ: Двоечник доволен тем, что опоздает в школу.",
        "Ответ: Специалисты были на ферме днем, а по ночам рядом репетировала рок-группа. Животные испытывали стресс, в результате чего возрастала щелочность мяса. Аналогичный случай описан в 'Детях капитана Гранта': по выражению Паганеля, 'вкусное мясо долго лежало, а невкусное - долго бежало'.",
        "Ответ: Водитель, сбив ночью прохожего, по документам определил его адрес и то, что он был одинок, и отвез его домой.",
        "Ответ: Одноногий негр.",
        "Ответ: А.Азимов, 'Поющий колокольчик'. Следователь бросает убийце бьющийся предмет. Тот не успел поймать его, так как его мышцы отвыкли от земной силы тяжести.",
        "Ответ: Профессор - женщина. Муж дерется с братом.",
        "Ответ: Этот человек был свидетелем того, что муж покупал в кассе только один обратный билет.",
        "Ответ: Самоубийца выбрал очень нестандартный способ свести счеты с жизнью: он бил себя по голове большой сосулькой до тех пор, пока не раскроил себе череп вдоль и поперек. После его смерти сосулька растаяла.",
        "Ответ: Дело происходит в доме второй (убитой) женщины. Чернокожий (простите, афро-американский) друг первой женщины был убит куклуксклановцами за несколько дней до описываемых событий. Зайдя в ванную, она случайно обнаружила среди грязного белья хозяйки униформу ку-клукс-клана, запачканную брызгами крови. Там же неподалеку валялся и пистолет.",
        "Ответ: Он - курьер, разносящий обыкновенную почту (газеты, журналы) по посольствам и дипломатическим представительствам. Как известно, территория посольства считается территорией иностранной державы.",
        "Ответ: Его убила монетка, которую кто-то из туристов кинул вниз с крыши высокого здания - Empire State Building.",
        "Ответ: Она ехала на мотоцикле без шлема. Когда автомобиль обогнал ее, длинные волосы женщины зацепились за автомобильную антенну. В результате этой аварии волосы были выдраны из головы вместе со скальпом.",
        "Ответ: Ответ: Он играл в 'Монополию'."
    ];
    var yesArr = [];
    function click(e) {
        _ii = e;
        _load();
    }
    function errclick() {
        _ii = 1;
        _load();
    }
    function save_show_rating(_ii) {
        document.getElementById("textStory" + _ii).style.display = "block";
        var greetingRating2 = [];
        greetingRating2[_ii] = Windows.Storage.ApplicationData.current.roamingSettings.values["greetingRating" + _ii];
        if (greetingRating2[_ii]) {
            document.getElementById("ratingControlDiv" + _ii).winControl.userRating = greetingRating2[_ii];
            document.getElementById("ratingOutput" + _ii).innerText = greetingRating2[_ii];
        }
        var outputValue = WinJS.Application.sessionState.greetingOutput;
        if (outputValue) {
            document.getElementById("greetingOutput" + _ii).innerText = outputValue;
        }

    
    }
    function _load() {

        for (var i = 1; i < 61; i++) {
            document.getElementById("textStory" + i).style.display = "none";
        }

        textStory.innerHTML = storyArr[_ii];
        textHeader.innerHTML = headerStoryArr[_ii];
        textAnswer.innerHTML = answerArr[_ii];

        if (_ii == 1) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged1, false);
        }
        if (_ii == 2) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged2, false);
        }
        if (_ii == 3) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged3, false);
        }
        if (_ii == 4) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged4, false);
        }
        if (_ii == 5) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged5, false);
        }
        if (_ii == 6) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged6, false);
        }
        if (_ii == 7) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged7, false);
        }
        if (_ii == 8) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged8, false);
        }
        if (_ii == 9) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged9, false);
        }
        if (_ii == 10) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged10, false);
        }
        if (_ii == 11) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged11, false);
        }
        if (_ii == 12) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged12, false);
        }
        if (_ii == 13) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged13, false);
        }
        if (_ii == 14) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged14, false);
        }
        if (_ii == 15) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged15, false);
        }
        if (_ii == 16) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged16, false);
        }
        if (_ii == 17) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged17, false);
        }
        if (_ii == 18) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged18, false);
        }
        if (_ii == 19) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged19, false);
        }
        if (_ii == 20) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged20, false);
        }
        if (_ii == 21) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged21, false);
        }
        if (_ii == 22) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged22, false);
        }
        if (_ii == 23) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged23, false);
        }
        if (_ii == 24) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged24, false);
        }
        if (_ii == 25) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged25, false);
        }
        if (_ii == 26) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged26, false);
        }
        if (_ii == 27) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged27, false);
        }
        if (_ii == 28) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged28, false);
        }
        if (_ii == 29) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged29, false);
        }
        if (_ii == 30) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged30, false);
        }
        if (_ii == 31) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged31, false);
        }
        if (_ii == 32) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged32, false);
        }
        if (_ii == 33) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged33, false);
        }
        if (_ii == 34) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged34, false);
        }
        if (_ii == 35) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged35, false);
        }
        if (_ii == 36) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged36, false);
        }
        if (_ii == 37) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged37, false);
        }
        if (_ii == 38) {
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged38, false);
            save_show_rating(_ii);
        }
        if (_ii == 39) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged39, false);
        }
        if (_ii == 40) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged40, false);
        }
        if (_ii == 41) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged41, false);
        }
        if (_ii == 42) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged42, false);
        }
        if (_ii == 43) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged43, false);
        }
        if (_ii == 44) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged44, false);
        }
        if (_ii == 45) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged45, false);
        }
        if (_ii == 46) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged46, false);
        }
        if (_ii == 47) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged47, false);
        }
        if (_ii == 48) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged48, false);
        }
        if (_ii == 49) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged49, false);
        }
        if (_ii == 50) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged50, false);
        }
        if (_ii == 51) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged51, false);
        }
        if (_ii == 52) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged52, false);
        }
        if (_ii == 53) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged53, false);
        }
        if (_ii == 54) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged54, false);
        }
        if (_ii == 55) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged55, false);
        }
        if (_ii == 56) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged56, false);
        }
        if (_ii == 57) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged57, false);
        }
        if (_ii == 58) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged58, false);
        }
        if (_ii == 59) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged59, false);
        }
        if (_ii == 60) {
            save_show_rating(_ii);
            document.getElementById("ratingControlDiv" + _ii).winControl.addEventListener("change", toggleChanged60, false);
        }


    }
    var _ii;
	function toggleExpandOrCollapse() {
	    if (textAnswer.style.display === "none") {
	        textAnswer.style.display = "block";
	        textAnswer.style.opacity = "1";
	        btnClick.src = "/images/btn-hide-answer.png";
	    } else {
	        textAnswer.style.opacity = "0";
	        textAnswer.style.display = "none";
	        btnClick.src = "/images/btn-answer.png";
	    }
	}
	function showAnswer() {
	    btnClick.addEventListener("click", toggleExpandOrCollapse, false);
	    textAnswer.style.display = "none";

	}
	function goToPageGameHome() {
	    document.location.href = "/default.html";
	}
	function goToPageHelp() {
	    document.location.href = "/html/help.html";
	}
	var page = WinJS.UI.Pages.define("openStoryPage.html", {
        ready: function (element, options) {

            local.readText("click.txt").done(function (fileContents) {
                if (fileContents == null) errclick();
                else click(fileContents);
            });
            help.onclick = function () { goToPageHelp();}
            backHome.addEventListener("click", goToPageGameHome, false);
            showAnswer();
            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);
            next.onclick = function (evt) {
                _ii++;
                if (_ii >= 61) { _ii = 60; _load(); }
                else { _load(); }
                textAnswer.style.opacity = "0";
                textAnswer.style.display = "none";
                btnClick.src = "/images/btn-answer.png";
            };
            back.onclick = function (evt) {
                _ii--;
                if (_ii <= 0) { _ii = 1; _load(); }
                else { _load(); }
                textAnswer.style.opacity = "0";
                textAnswer.style.display = "none";
                btnClick.src = "/images/btn-answer.png";
            };

            document.onmousedown = nocopy;
            document.onmouseup = nocopy;
            document.onmousemove = nocopy;
            document.ondragstart = nocopy;
            document.onselectstart = nocopy;
            document.ontextmenu = nocopy;
            document.oncopy = nocopy;

            var outputValue1 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue1) {
                var greetingOutput1 = document.getElementById("1");
                greetingOutput1.innerText = outputValue1;
            }
            ///end
            ///begin
            var outputValue2 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue2) {
                var greetingOutput2 = document.getElementById("2");
                greetingOutput2.innerText = outputValue2;
            }
            ///end
            ///begin
            var outputValue3 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue3) {
                var greetingOutput3 = document.getElementById("3");
                greetingOutput3.innerText = outputValue3;
            }
            ///end
            ///begin
            var outputValue4 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue4) {
                var greetingOutput4 = document.getElementById("4");
                greetingOutput4.innerText = outputValue4;
            }
            ///end
            ///begin
            var outputValue5 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue5) {
                var greetingOutput5 = document.getElementById("5");
                greetingOutput5.innerText = outputValue5;
            }
            ///end
            ///begin
            var outputValue6 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue6) {
                var greetingOutput6 = document.getElementById("6");
                greetingOutput6.innerText = outputValue6;
            }
            ///end
            ///begin
            var outputValue7 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue7) {
                var greetingOutput7 = document.getElementById("7");
                greetingOutput7.innerText = outputValue7;
            }
            ///end
            ///begin
            var outputValue8 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue8) {
                var greetingOutput8 = document.getElementById("8");
                greetingOutput8.innerText = outputValue8;
            }
            ///end
            ///begin
            var outputValue9 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue9) {
                var greetingOutput9 = document.getElementById("9");
                greetingOutput9.innerText = outputValue9;
            }
            ///end
            ///begin
            var outputValue10 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue10) {
                var greetingOutput10 = document.getElementById("10");
                greetingOutput10.innerText = outputValue10;
            }
            ///end
            ///begin
            var outputValue11 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue11) {
                var greetingOutput11 = document.getElementById("11");
                greetingOutput11.innerText = outputValue11;
            }
            ///end
            ///begin
            var outputValue12 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue12) {
                var greetingOutput12 = document.getElementById("12");
                greetingOutput12.innerText = outputValue12;
            }
            ///end
            ///begin
            var outputValue13 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue13) {
                var greetingOutput13 = document.getElementById("13");
                greetingOutput13.innerText = outputValue13;
            }
            ///end
            ///begin
            var outputValue14 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue14) {
                var greetingOutput14 = document.getElementById("14");
                greetingOutput14.innerText = outputValue14;
            }
            ///end
            ///begin
            var outputValue15 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue15) {
                var greetingOutput15 = document.getElementById("15");
                greetingOutput15.innerText = outputValue15;
            }
            ///end
            ///begin
            var outputValue16 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue16) {
                var greetingOutput16 = document.getElementById("16");
                greetingOutput16.innerText = outputValue16;
            }
            ///end
            ///begin
            var outputValue17 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue17) {
                var greetingOutput17 = document.getElementById("17");
                greetingOutput17.innerText = outputValue17;
            }
            ///end
            ///begin
            var outputValue18 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue18) {
                var greetingOutput18 = document.getElementById("18");
                greetingOutput18.innerText = outputValue18;
            }
            ///end
            ///begin
            var outputValue19 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue19) {
                var greetingOutput19 = document.getElementById("19");
                greetingOutput19.innerText = outputValue19;
            }
            ///end
            ///begin
            var outputValue20 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue20) {
                var greetingOutput20 = document.getElementById("20");
                greetingOutput20.innerText = outputValue20;
            }
            ///end
            ///begin
            var outputValue21 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue21) {
                var greetingOutput21 = document.getElementById("21");
                greetingOutput21.innerText = outputValue21;
            }
            ///end
            ///begin
            var outputValue22 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue22) {
                var greetingOutput22 = document.getElementById("22");
                greetingOutput22.innerText = outputValue22;
            }
            ///end
            ///begin
            var outputValue23 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue23) {
                var greetingOutput23 = document.getElementById("23");
                greetingOutput23.innerText = outputValue23;
            }
            ///end
            ///begin
            var outputValue24 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue24) {
                var greetingOutput24 = document.getElementById("24");
                greetingOutput24.innerText = outputValue24;
            }
            ///end
            ///begin
            var outputValue25 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue25) {
                var greetingOutput25 = document.getElementById("25");
                greetingOutput25.innerText = outputValue25;
            }
            ///end
            ///begin
            var outputValue26 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue26) {
                var greetingOutput26 = document.getElementById("26");
                greetingOutput26.innerText = outputValue26;
            }
            ///end
            ///begin
            var outputValue27 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue27) {
                var greetingOutput27 = document.getElementById("27");
                greetingOutput27.innerText = outputValue27;
            }
            ///end
            ///begin
            var outputValue28 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue28) {
                var greetingOutput28 = document.getElementById("28");
                greetingOutput28.innerText = outputValue28;
            }
            ///end
            ///begin
            var outputValue29 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue29) {
                var greetingOutput29 = document.getElementById("29");
                greetingOutput29.innerText = outputValue29;
            }
            ///end
            ///begin
            var outputValue30 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue30) {
                var greetingOutput30 = document.getElementById("30");
                greetingOutput30.innerText = outputValue30;
            }
            ///end
            ///begin
            var outputValue31 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue31) {
                var greetingOutput31 = document.getElementById("31");
                greetingOutput31.innerText = outputValue31;
            }
            ///end
            ///begin
            var outputValue32 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue32) {
                var greetingOutput32 = document.getElementById("32");
                greetingOutput32.innerText = outputValue32;
            }
            ///end
            ///begin
            var outputValue33 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue33) {
                var greetingOutput33 = document.getElementById("33");
                greetingOutput33.innerText = outputValue33;
            }
            ///end
            ///begin
            var outputValue34 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue34) {
                var greetingOutput34 = document.getElementById("34");
                greetingOutput34.innerText = outputValue34;
            }
            ///end
            ///begin
            var outputValue35 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue35) {
                var greetingOutput55 = document.getElementById("35");
                greetingOutput35.innerText = outputValue35;
            }
            ///end
            ///begin
            var outputValue36 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue36) {
                var greetingOutput36 = document.getElementById("36");
                greetingOutput36.innerText = outputValue36;
            }
            ///end
            ///begin
            var outputValue37 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue37) {
                var greetingOutput37 = document.getElementById("37");
                greetingOutput37.innerText = outputValue37;
            }
            ///end
            ///begin
            var outputValue38 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue38) {
                var greetingOutput38 = document.getElementById("38");
                greetingOutput38.innerText = outputValue38;
            }
            ///end
            ///begin
            var outputValue39 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue39) {
                var greetingOutput39 = document.getElementById("39");
                greetingOutput39.innerText = outputValue39;
            }
            ///end
            ///begin
            var outputValue40 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue40) {
                var greetingOutput40 = document.getElementById("40");
                greetingOutput40.innerText = outputValue40;
            }
            ///end
            ///begin
            var outputValue41 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue41) {
                var greetingOutput41 = document.getElementById("41");
                greetingOutput41.innerText = outputValue41;
            }
            ///end
            ///begin
            var outputValue42 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue42) {
                var greetingOutput42 = document.getElementById("42");
                greetingOutput42.innerText = outputValue42;
            }
            ///end
            ///begin
            var outputValue43 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue34) {
                var greetingOutput43 = document.getElementById("43");
                greetingOutput43.innerText = outputValue43;
            }
            ///end
            ///begin
            var outputValue44 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue44) {
                var greetingOutput44 = document.getElementById("44");
                greetingOutput44.innerText = outputValue44;
            }
            ///end
            ///begin
            var outputValue45 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue45) {
                var greetingOutput45 = document.getElementById("45");
                greetingOutput45.innerText = outputValue45;
            }
            ///end
            ///begin
            var outputValue46 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue46) {
                var greetingOutput46 = document.getElementById("46");
                greetingOutput46.innerText = outputValue46;
            }
            ///end
            ///begin
            var outputValue47 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue47) {
                var greetingOutput47 = document.getElementById("47");
                greetingOutput47.innerText = outputValue47;
            }
            ///end
            ///begin
            var outputValue48 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue48) {
                var greetingOutput48 = document.getElementById("48");
                greetingOutput48.innerText = outputValue48;
            }
            ///end
            ///begin
            var outputValue49 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue49) {
                var greetingOutput49 = document.getElementById("49");
                greetingOutput49.innerText = outputValue49;
            }
            ///end
            ///begin
            var outputValue50 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue50) {
                var greetingOutput50 = document.getElementById("50");
                greetingOutput50.innerText = outputValue50;
            }
            ///end
            ///begin
            var outputValue51 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue51) {
                var greetingOutput51 = document.getElementById("51");
                greetingOutput51.innerText = outputValue51;
            }
            ///end
            ///begin
            var outputValue52 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue52) {
                var greetingOutput52 = document.getElementById("52");
                greetingOutput52.innerText = outputValue52;
            }
            ///end
            ///begin
            var outputValue53 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue53) {
                var greetingOutput53 = document.getElementById("53");
                greetingOutput53.innerText = outputValue53;
            }
            ///end
            ///begin
            var outputValue54 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue54) {
                var greetingOutput54 = document.getElementById("54");
                greetingOutput54.innerText = outputValue54;
            }
            ///end
            ///begin
            var outputValue55 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue55) {
                var greetingOutput55 = document.getElementById("55");
                greetingOutput55.innerText = outputValue55;
            }
            ///end
            ///begin
            var outputValue56 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue56) {
                var greetingOutput56 = document.getElementById("56");
                greetingOutput56.innerText = outputValue56;
            }
            ///end
            ///begin
            var outputValue57 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue57) {
                var greetingOutput57 = document.getElementById("57");
                greetingOutput57.innerText = outputValue57;
            }
            ///end
            ///begin
            var outputValue58 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue58) {
                var greetingOutput58 = document.getElementById("58");
                greetingOutput58.innerText = outputValue58;
            }
            ///end
            ///begin
            var outputValue59 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue59) {
                var greetingOutput59 = document.getElementById("59");
                greetingOutput59.innerText = outputValue59;
            }
            ///end
            ///begin
            var outputValue60 = WinJS.Application.sessionState.greetingOutput;
            if (outputValue60) {
                var greetingOutput60 = document.getElementById("60");
                greetingOutput60.innerText = outputValue60;
            }
            ///end
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            ///begin
            var toggleControlDiv1 = document.getElementById("ratingControlDiv1");// Retrieve the div that hosts the Toggle control.
            var toggleControl1 = toggleControlDiv1.winControl;// Retrieve the actual Toggle control.
            toggleControl1.addEventListener("change", toggleChanged1); // Register the event handler. 
            var roamingSettings1 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection1 = roamingSettings1.values["toggleSelection1"];// Restore the Toggle selection.
            if (toggleSelection1) {
                toggleControl1.checked = toggleSelection1;
                var greetingOutput1 = document.getElementById("1");// Apply the properties of the toggle selection
                if (toggleControl1.checked == true) { greetingOutput1.setAttribute("class", "toggle-on"); } else { greetingOutput1.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv2 = document.getElementById("ratingControlDiv2");// Retrieve the div that hosts the Toggle control.
            var toggleControl2 = toggleControlDiv2.winControl;// Retrieve the actual Toggle control.
            toggleControl2.addEventListener("change", toggleChanged2); // Register the event handler. 
            var roamingSettings2 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection2 = roamingSettings2.values["toggleSelection2"];// Restore the Toggle selection.
            if (toggleSelection2) {
                toggleControl2.checked = toggleSelection2;
                var greetingOutput2 = document.getElementById("2");// Apply the properties of the toggle selection
                if (toggleControl2.checked == true) { greetingOutput2.setAttribute("class", "toggle-on"); } else { greetingOutput2.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv3 = document.getElementById("ratingControlDiv3");// Retrieve the div that hosts the Toggle control.
            var toggleControl3 = toggleControlDiv3.winControl;// Retrieve the actual Toggle control.
            toggleControl3.addEventListener("change", toggleChanged3); // Register the event handler. 
            var roamingSettings3 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection3 = roamingSettings3.values["toggleSelection3"];// Restore the Toggle selection.
            if (toggleSelection3) {
                toggleControl3.checked = toggleSelection3;
                var greetingOutput3 = document.getElementById("3");// Apply the properties of the toggle selection
                if (toggleControl3.checked == true) { greetingOutput3.setAttribute("class", "toggle-on"); } else { greetingOutput3.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv4 = document.getElementById("ratingControlDiv4");// Retrieve the div that hosts the Toggle control.
            var toggleControl4 = toggleControlDiv4.winControl;// Retrieve the actual Toggle control.
            toggleControl4.addEventListener("change", toggleChanged4); // Register the event handler. 
            var roamingSettings4 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection4 = roamingSettings4.values["toggleSelection4"];// Restore the Toggle selection.
            if (toggleSelection4) {
                toggleControl4.checked = toggleSelection4;
                var greetingOutput4 = document.getElementById("4");// Apply the properties of the toggle selection
                if (toggleControl4.checked == true) { greetingOutput4.setAttribute("class", "toggle-on"); } else { greetingOutput4.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv5 = document.getElementById("ratingControlDiv5");// Retrieve the div that hosts the Toggle control.
            var toggleControl5 = toggleControlDiv5.winControl;// Retrieve the actual Toggle control.
            toggleControl5.addEventListener("change", toggleChanged5); // Register the event handler. 
            var roamingSettings5 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection5 = roamingSettings5.values["toggleSelection5"];// Restore the Toggle selection.
            if (toggleSelection5) {
                toggleControl5.checked = toggleSelection5;
                var greetingOutput5 = document.getElementById("5");// Apply the properties of the toggle selection
                if (toggleControl5.checked == true) { greetingOutput5.setAttribute("class", "toggle-on"); } else { greetingOutput5.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv6 = document.getElementById("ratingControlDiv6");// Retrieve the div that hosts the Toggle control.
            var toggleControl6 = toggleControlDiv6.winControl;// Retrieve the actual Toggle control.
            toggleControl6.addEventListener("change", toggleChanged6); // Register the event handler. 
            var roamingSettings6 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection6 = roamingSettings6.values["toggleSelection6"];// Restore the Toggle selection.
            if (toggleSelection6) {
                toggleControl6.checked = toggleSelection6;
                var greetingOutput6 = document.getElementById("6");// Apply the properties of the toggle selection
                if (toggleControl6.checked == true) { greetingOutput6.setAttribute("class", "toggle-on"); } else { greetingOutput6.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv7 = document.getElementById("ratingControlDiv7");// Retrieve the div that hosts the Toggle control.
            var toggleControl7 = toggleControlDiv7.winControl;// Retrieve the actual Toggle control.
            toggleControl7.addEventListener("change", toggleChanged7); // Register the event handler. 
            var roamingSettings7 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection7 = roamingSettings7.values["toggleSelection7"];// Restore the Toggle selection.
            if (toggleSelection7) {
                toggleControl7.checked = toggleSelection7;
                var greetingOutput7 = document.getElementById("7");// Apply the properties of the toggle selection
                if (toggleControl7.checked == true) { greetingOutput7.setAttribute("class", "toggle-on"); } else { greetingOutput7.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv8 = document.getElementById("ratingControlDiv8");// Retrieve the div that hosts the Toggle control.
            var toggleControl8 = toggleControlDiv8.winControl;// Retrieve the actual Toggle control.
            toggleControl8.addEventListener("change", toggleChanged8); // Register the event handler. 
            var roamingSettings8 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection8 = roamingSettings8.values["toggleSelection8"];// Restore the Toggle selection.
            if (toggleSelection8) {
                toggleControl8.checked = toggleSelection8;
                var greetingOutput8 = document.getElementById("8");// Apply the properties of the toggle selection
                if (toggleControl8.checked == true) { greetingOutput8.setAttribute("class", "toggle-on"); } else { greetingOutput8.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv9 = document.getElementById("ratingControlDiv9");// Retrieve the div that hosts the Toggle control.
            var toggleControl9 = toggleControlDiv9.winControl;// Retrieve the actual Toggle control.
            toggleControl9.addEventListener("change", toggleChanged9); // Register the event handler. 
            var roamingSettings9 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection9 = roamingSettings9.values["toggleSelection9"];// Restore the Toggle selection.
            if (toggleSelection9) {
                toggleControl9.checked = toggleSelection9;
                var greetingOutput9 = document.getElementById("9");// Apply the properties of the toggle selection
                if (toggleControl9.checked == true) { greetingOutput9.setAttribute("class", "toggle-on"); } else { greetingOutput9.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv10 = document.getElementById("ratingControlDiv10");// Retrieve the div that hosts the Toggle control.
            var toggleControl10 = toggleControlDiv10.winControl;// Retrieve the actual Toggle control.
            toggleControl10.addEventListener("change", toggleChanged10); // Register the event handler. 
            var roamingSettings10 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection10 = roamingSettings10.values["toggleSelection10"];// Restore the Toggle selection.
            if (toggleSelection10) {
                toggleControl10.checked = toggleSelection10;
                var greetingOutput10 = document.getElementById("10");// Apply the properties of the toggle selection
                if (toggleControl10.checked == true) { greetingOutput10.setAttribute("class", "toggle-on"); } else { greetingOutput10.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv11 = document.getElementById("ratingControlDiv11");// Retrieve the div that hosts the Toggle control.
            var toggleControl11 = toggleControlDiv11.winControl;// Retrieve the actual Toggle control.
            toggleControl11.addEventListener("change", toggleChanged11); // Register the event handler. 
            var roamingSettings11 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection11 = roamingSettings11.values["toggleSelection11"];// Restore the Toggle selection.
            if (toggleSelection11) {
                toggleControl11.checked = toggleSelection11;
                var greetingOutput11 = document.getElementById("11");// Apply the properties of the toggle selection
                if (toggleControl11.checked == true) { greetingOutput11.setAttribute("class", "toggle-on"); } else { greetingOutput11.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv12 = document.getElementById("ratingControlDiv12");// Retrieve the div that hosts the Toggle control.
            var toggleControl12 = toggleControlDiv12.winControl;// Retrieve the actual Toggle control.
            toggleControl12.addEventListener("change", toggleChanged12); // Register the event handler. 
            var roamingSettings12 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection12 = roamingSettings12.values["toggleSelection4"];// Restore the Toggle selection.
            if (toggleSelection12) {
                toggleControl12.checked = toggleSelection12;
                var greetingOutput12 = document.getElementById("12");// Apply the properties of the toggle selection
                if (toggleControl12.checked == true) { greetingOutput12.setAttribute("class", "toggle-on"); } else { greetingOutput12.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv13 = document.getElementById("ratingControlDiv13");// Retrieve the div that hosts the Toggle control.
            var toggleControl13 = toggleControlDiv13.winControl;// Retrieve the actual Toggle control.
            toggleControl13.addEventListener("change", toggleChanged13); // Register the event handler. 
            var roamingSettings13 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection13 = roamingSettings13.values["toggleSelection13"];// Restore the Toggle selection.
            if (toggleSelection13) {
                toggleControl13.checked = toggleSelection13;
                var greetingOutput13 = document.getElementById("13");// Apply the properties of the toggle selection
                if (toggleControl13.checked == true) { greetingOutput13.setAttribute("class", "toggle-on"); } else { greetingOutput13.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv14 = document.getElementById("ratingControlDiv14");// Retrieve the div that hosts the Toggle control.
            var toggleControl14 = toggleControlDiv14.winControl;// Retrieve the actual Toggle control.
            toggleControl14.addEventListener("change", toggleChanged14); // Register the event handler. 
            var roamingSettings14 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection14 = roamingSettings14.values["toggleSelection14"];// Restore the Toggle selection.
            if (toggleSelection14) {
                toggleControl14.checked = toggleSelection14;
                var greetingOutput14 = document.getElementById("14");// Apply the properties of the toggle selection
                if (toggleControl14.checked == true) { greetingOutput14.setAttribute("class", "toggle-on"); } else { greetingOutput14.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv15 = document.getElementById("ratingControlDiv15");// Retrieve the div that hosts the Toggle control.
            var toggleControl15 = toggleControlDiv15.winControl;// Retrieve the actual Toggle control.
            toggleControl15.addEventListener("change", toggleChanged15); // Register the event handler. 
            var roamingSettings15 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection15 = roamingSettings15.values["toggleSelection15"];// Restore the Toggle selection.
            if (toggleSelection15) {
                toggleControl15.checked = toggleSelection15;
                var greetingOutput15 = document.getElementById("15");// Apply the properties of the toggle selection
                if (toggleControl15.checked == true) { greetingOutput15.setAttribute("class", "toggle-on"); } else { greetingOutput15.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv16 = document.getElementById("ratingControlDiv16");// Retrieve the div that hosts the Toggle control.
            var toggleControl16 = toggleControlDiv16.winControl;// Retrieve the actual Toggle control.
            toggleControl16.addEventListener("change", toggleChanged16); // Register the event handler. 
            var roamingSettings16 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection16 = roamingSettings16.values["toggleSelection16"];// Restore the Toggle selection.
            if (toggleSelection16) {
                toggleControl16.checked = toggleSelection16;
                var greetingOutput16 = document.getElementById("16");// Apply the properties of the toggle selection
                if (toggleControl16.checked == true) { greetingOutput16.setAttribute("class", "toggle-on"); } else { greetingOutput16.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv17 = document.getElementById("ratingControlDiv17");// Retrieve the div that hosts the Toggle control.
            var toggleControl17 = toggleControlDiv17.winControl;// Retrieve the actual Toggle control.
            toggleControl17.addEventListener("change", toggleChanged17); // Register the event handler. 
            var roamingSettings17 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection17 = roamingSettings17.values["toggleSelection17"];// Restore the Toggle selection.
            if (toggleSelection17) {
                toggleControl17.checked = toggleSelection17;
                var greetingOutput17 = document.getElementById("17");// Apply the properties of the toggle selection
                if (toggleControl17.checked == true) { greetingOutput17.setAttribute("class", "toggle-on"); } else { greetingOutput17.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv18 = document.getElementById("ratingControlDiv18");// Retrieve the div that hosts the Toggle control.
            var toggleControl18 = toggleControlDiv18.winControl;// Retrieve the actual Toggle control.
            toggleControl18.addEventListener("change", toggleChanged18); // Register the event handler. 
            var roamingSettings18 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection18 = roamingSettings18.values["toggleSelection18"];// Restore the Toggle selection.
            if (toggleSelection18) {
                toggleControl18.checked = toggleSelection18;
                var greetingOutput18 = document.getElementById("18");// Apply the properties of the toggle selection
                if (toggleControl18.checked == true) { greetingOutput18.setAttribute("class", "toggle-on"); } else { greetingOutput18.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv19 = document.getElementById("ratingControlDiv19");// Retrieve the div that hosts the Toggle control.
            var toggleControl19 = toggleControlDiv19.winControl;// Retrieve the actual Toggle control.
            toggleControl19.addEventListener("change", toggleChanged19); // Register the event handler. 
            var roamingSettings19 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection19 = roamingSettings19.values["toggleSelection19"];// Restore the Toggle selection.
            if (toggleSelection19) {
                toggleControl19.checked = toggleSelection19;
                var greetingOutput19 = document.getElementById("19");// Apply the properties of the toggle selection
                if (toggleControl19.checked == true) { greetingOutput19.setAttribute("class", "toggle-on"); } else { greetingOutput19.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv20 = document.getElementById("ratingControlDiv20");// Retrieve the div that hosts the Toggle control.
            var toggleControl20 = toggleControlDiv20.winControl;// Retrieve the actual Toggle control.
            toggleControl20.addEventListener("change", toggleChanged20); // Register the event handler. 
            var roamingSettings20 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection20 = roamingSettings20.values["toggleSelection20"];// Restore the Toggle selection.
            if (toggleSelection20) {
                toggleControl20.checked = toggleSelection20;
                var greetingOutput20 = document.getElementById("20");// Apply the properties of the toggle selection
                if (toggleControl20.checked == true) { greetingOutput20.setAttribute("class", "toggle-on"); } else { greetingOutput20.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv21 = document.getElementById("ratingControlDiv21");// Retrieve the div that hosts the Toggle control.
            var toggleControl21 = toggleControlDiv21.winControl;// Retrieve the actual Toggle control.
            toggleControl21.addEventListener("change", toggleChanged21); // Register the event handler. 
            var roamingSettings21 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection21 = roamingSettings21.values["toggleSelection21"];// Restore the Toggle selection.
            if (toggleSelection21) {
                toggleControl21.checked = toggleSelection21;
                var greetingOutput21 = document.getElementById("21");// Apply the properties of the toggle selection
                if (toggleControl21.checked == true) { greetingOutput21.setAttribute("class", "toggle-on"); } else { greetingOutput21.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv22 = document.getElementById("ratingControlDiv22");// Retrieve the div that hosts the Toggle control.
            var toggleControl22 = toggleControlDiv22.winControl;// Retrieve the actual Toggle control.
            toggleControl22.addEventListener("change", toggleChanged22); // Register the event handler. 
            var roamingSettings22 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection22 = roamingSettings22.values["toggleSelection22"];// Restore the Toggle selection.
            if (toggleSelection22) {
                toggleControl22.checked = toggleSelection4;
                var greetingOutput22 = document.getElementById("22");// Apply the properties of the toggle selection
                if (toggleControl22.checked == true) { greetingOutput22.setAttribute("class", "toggle-on"); } else { greetingOutput22.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv23 = document.getElementById("ratingControlDiv23");// Retrieve the div that hosts the Toggle control.
            var toggleControl23 = toggleControlDiv23.winControl;// Retrieve the actual Toggle control.
            toggleControl23.addEventListener("change", toggleChanged23); // Register the event handler. 
            var roamingSettings23 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection23 = roamingSettings23.values["toggleSelection23"];// Restore the Toggle selection.
            if (toggleSelection23) {
                toggleControl23.checked = toggleSelection23;
                var greetingOutput23 = document.getElementById("23");// Apply the properties of the toggle selection
                if (toggleControl23.checked == true) { greetingOutput23.setAttribute("class", "toggle-on"); } else { greetingOutput23.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv24 = document.getElementById("ratingControlDiv24");// Retrieve the div that hosts the Toggle control.
            var toggleControl24 = toggleControlDiv24.winControl;// Retrieve the actual Toggle control.
            toggleControl24.addEventListener("change", toggleChanged24); // Register the event handler. 
            var roamingSettings24 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection24 = roamingSettings24.values["toggleSelection24"];// Restore the Toggle selection.
            if (toggleSelection24) {
                toggleControl24.checked = toggleSelection24;
                var greetingOutput24 = document.getElementById("24");// Apply the properties of the toggle selection
                if (toggleControl24.checked == true) { greetingOutput24.setAttribute("class", "toggle-on"); } else { greetingOutput24.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv25 = document.getElementById("ratingControlDiv25");// Retrieve the div that hosts the Toggle control.
            var toggleControl25 = toggleControlDiv25.winControl;// Retrieve the actual Toggle control.
            toggleControl25.addEventListener("change", toggleChanged25); // Register the event handler. 
            var roamingSettings25 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection25 = roamingSettings25.values["toggleSelection25"];// Restore the Toggle selection.
            if (toggleSelection25) {
                toggleControl25.checked = toggleSelection25;
                var greetingOutput25 = document.getElementById("25");// Apply the properties of the toggle selection
                if (toggleControl25.checked == true) { greetingOutput25.setAttribute("class", "toggle-on"); } else { greetingOutput25.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv26 = document.getElementById("ratingControlDiv26");// Retrieve the div that hosts the Toggle control.
            var toggleControl26 = toggleControlDiv26.winControl;// Retrieve the actual Toggle control.
            toggleControl26.addEventListener("change", toggleChanged26); // Register the event handler. 
            var roamingSettings26 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection26 = roamingSettings26.values["toggleSelection26"];// Restore the Toggle selection.
            if (toggleSelection26) {
                toggleControl26.checked = toggleSelection26;
                var greetingOutput26 = document.getElementById("26");// Apply the properties of the toggle selection
                if (toggleControl26.checked == true) { greetingOutput26.setAttribute("class", "toggle-on"); } else { greetingOutput26.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv27 = document.getElementById("ratingControlDiv27");// Retrieve the div that hosts the Toggle control.
            var toggleControl27 = toggleControlDiv27.winControl;// Retrieve the actual Toggle control.
            toggleControl27.addEventListener("change", toggleChanged27); // Register the event handler. 
            var roamingSettings27 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection27 = roamingSettings27.values["toggleSelection27"];// Restore the Toggle selection.
            if (toggleSelection27) {
                toggleControl27.checked = toggleSelection27;
                var greetingOutput27 = document.getElementById("27");// Apply the properties of the toggle selection
                if (toggleControl27.checked == true) { greetingOutput27.setAttribute("class", "toggle-on"); } else { greetingOutput27.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv28 = document.getElementById("ratingControlDiv28");// Retrieve the div that hosts the Toggle control.
            var toggleControl28 = toggleControlDiv28.winControl;// Retrieve the actual Toggle control.
            toggleControl28.addEventListener("change", toggleChanged28); // Register the event handler. 
            var roamingSettings28 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection28 = roamingSettings28.values["toggleSelection28"];// Restore the Toggle selection.
            if (toggleSelection28) {
                toggleControl28.checked = toggleSelection28;
                var greetingOutput28 = document.getElementById("28");// Apply the properties of the toggle selection
                if (toggleControl28.checked == true) { greetingOutput28.setAttribute("class", "toggle-on"); } else { greetingOutput28.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv29 = document.getElementById("ratingControlDiv29");// Retrieve the div that hosts the Toggle control.
            var toggleControl29 = toggleControlDiv29.winControl;// Retrieve the actual Toggle control.
            toggleControl29.addEventListener("change", toggleChanged29); // Register the event handler. 
            var roamingSettings29 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection29 = roamingSettings29.values["toggleSelection29"];// Restore the Toggle selection.
            if (toggleSelection29) {
                toggleControl29.checked = toggleSelection29;
                var greetingOutput29 = document.getElementById("29");// Apply the properties of the toggle selection
                if (toggleControl29.checked == true) { greetingOutput29.setAttribute("class", "toggle-on"); } else { greetingOutput29.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv30 = document.getElementById("ratingControlDiv30");// Retrieve the div that hosts the Toggle control.
            var toggleControl30 = toggleControlDiv30.winControl;// Retrieve the actual Toggle control.
            toggleControl30.addEventListener("change", toggleChanged30); // Register the event handler. 
            var roamingSettings30 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection30 = roamingSettings30.values["toggleSelection30"];// Restore the Toggle selection.
            if (toggleSelection30) {
                toggleControl30.checked = toggleSelection30;
                var greetingOutput30 = document.getElementById("30");// Apply the properties of the toggle selection
                if (toggleControl30.checked == true) { greetingOutput30.setAttribute("class", "toggle-on"); } else { greetingOutput30.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv31 = document.getElementById("ratingControlDiv31");// Retrieve the div that hosts the Toggle control.
            var toggleControl31 = toggleControlDiv31.winControl;// Retrieve the actual Toggle control.
            toggleControl31.addEventListener("change", toggleChanged31); // Register the event handler. 
            var roamingSettings31 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection31 = roamingSettings31.values["toggleSelection31"];// Restore the Toggle selection.
            if (toggleSelection31) {
                toggleControl31.checked = toggleSelection31;
                var greetingOutput31 = document.getElementById("31");// Apply the properties of the toggle selection
                if (toggleControl31.checked == true) { greetingOutput31.setAttribute("class", "toggle-on"); } else { greetingOutput31.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv32 = document.getElementById("ratingControlDiv32");// Retrieve the div that hosts the Toggle control.
            var toggleControl32 = toggleControlDiv32.winControl;// Retrieve the actual Toggle control.
            toggleControl32.addEventListener("change", toggleChanged32); // Register the event handler. 
            var roamingSettings32 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection32 = roamingSettings32.values["toggleSelection32"];// Restore the Toggle selection.
            if (toggleSelection32) {
                toggleControl32.checked = toggleSelection32;
                var greetingOutput32 = document.getElementById("32");// Apply the properties of the toggle selection
                if (toggleControl32.checked == true) { greetingOutput32.setAttribute("class", "toggle-on"); } else { greetingOutput32.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv33 = document.getElementById("ratingControlDiv33");// Retrieve the div that hosts the Toggle control.
            var toggleControl33 = toggleControlDiv33.winControl;// Retrieve the actual Toggle control.
            toggleControl33.addEventListener("change", toggleChanged33); // Register the event handler. 
            var roamingSettings33 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection33 = roamingSettings33.values["toggleSelection33"];// Restore the Toggle selection.
            if (toggleSelection33) {
                toggleControl33.checked = toggleSelection33;
                var greetingOutput33 = document.getElementById("33");// Apply the properties of the toggle selection
                if (toggleControl33.checked == true) { greetingOutput33.setAttribute("class", "toggle-on"); } else { greetingOutput33.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv34 = document.getElementById("ratingControlDiv34");// Retrieve the div that hosts the Toggle control.
            var toggleControl34 = toggleControlDiv34.winControl;// Retrieve the actual Toggle control.
            toggleControl34.addEventListener("change", toggleChanged34); // Register the event handler. 
            var roamingSettings34 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection34 = roamingSettings34.values["toggleSelection34"];// Restore the Toggle selection.
            if (toggleSelection34) {
                toggleControl34.checked = toggleSelection34;
                var greetingOutput34 = document.getElementById("34");// Apply the properties of the toggle selection
                if (toggleControl34.checked == true) { greetingOutput34.setAttribute("class", "toggle-on"); } else { greetingOutput34.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv35 = document.getElementById("ratingControlDiv35");// Retrieve the div that hosts the Toggle control.
            var toggleControl35 = toggleControlDiv35.winControl;// Retrieve the actual Toggle control.
            toggleControl35.addEventListener("change", toggleChanged35); // Register the event handler. 
            var roamingSettings35 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection35 = roamingSettings35.values["toggleSelection35"];// Restore the Toggle selection.
            if (toggleSelection35) {
                toggleControl35.checked = toggleSelection35;
                var greetingOutput35 = document.getElementById("35");// Apply the properties of the toggle selection
                if (toggleControl35.checked == true) { greetingOutput35.setAttribute("class", "toggle-on"); } else { greetingOutput35.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv36 = document.getElementById("ratingControlDiv36");// Retrieve the div that hosts the Toggle control.
            var toggleControl36 = toggleControlDiv36.winControl;// Retrieve the actual Toggle control.
            toggleControl36.addEventListener("change", toggleChanged36); // Register the event handler. 
            var roamingSettings36 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection36 = roamingSettings36.values["toggleSelection36"];// Restore the Toggle selection.
            if (toggleSelection36) {
                toggleControl36.checked = toggleSelection36;
                var greetingOutput36 = document.getElementById("36");// Apply the properties of the toggle selection
                if (toggleControl36.checked == true) { greetingOutput36.setAttribute("class", "toggle-on"); } else { greetingOutput36.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv37 = document.getElementById("ratingControlDiv37");// Retrieve the div that hosts the Toggle control.
            var toggleControl37 = toggleControlDiv37.winControl;// Retrieve the actual Toggle control.
            toggleControl37.addEventListener("change", toggleChanged37); // Register the event handler. 
            var roamingSettings37 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection37 = roamingSettings37.values["toggleSelection37"];// Restore the Toggle selection.
            if (toggleSelection37) {
                toggleControl37.checked = toggleSelection37;
                var greetingOutput37 = document.getElementById("37");// Apply the properties of the toggle selection
                if (toggleControl37.checked == true) { greetingOutput37.setAttribute("class", "toggle-on"); } else { greetingOutput37.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv38 = document.getElementById("ratingControlDiv38");// Retrieve the div that hosts the Toggle control.
            var toggleControl38 = toggleControlDiv38.winControl;// Retrieve the actual Toggle control.
            toggleControl38.addEventListener("change", toggleChanged38); // Register the event handler. 
            var roamingSettings38 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection38 = roamingSettings38.values["toggleSelection38"];// Restore the Toggle selection.
            if (toggleSelection38) {
                toggleControl38.checked = toggleSelection38;
                var greetingOutput38 = document.getElementById("38");// Apply the properties of the toggle selection
                if (toggleControl38.checked == true) { greetingOutput38.setAttribute("class", "toggle-on"); } else { greetingOutput38.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv39 = document.getElementById("ratingControlDiv39");// Retrieve the div that hosts the Toggle control.
            var toggleControl39 = toggleControlDiv39.winControl;// Retrieve the actual Toggle control.
            toggleControl39.addEventListener("change", toggleChanged39); // Register the event handler. 
            var roamingSettings39 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection39 = roamingSettings39.values["toggleSelection39"];// Restore the Toggle selection.
            if (toggleSelection39) {
                toggleControl39.checked = toggleSelection38;
                var greetingOutput39 = document.getElementById("39");// Apply the properties of the toggle selection
                if (toggleControl39.checked == true) { greetingOutput39.setAttribute("class", "toggle-on"); } else { greetingOutput39.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv40 = document.getElementById("ratingControlDiv40");// Retrieve the div that hosts the Toggle control.
            var toggleControl40 = toggleControlDiv40.winControl;// Retrieve the actual Toggle control.
            toggleControl40.addEventListener("change", toggleChanged40); // Register the event handler. 
            var roamingSettings40 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection40 = roamingSettings40.values["toggleSelection4"];// Restore the Toggle selection.
            if (toggleSelection40) {
                toggleControl40.checked = toggleSelection40;
                var greetingOutput40 = document.getElementById("40");// Apply the properties of the toggle selection
                if (toggleControl40.checked == true) { greetingOutput40.setAttribute("class", "toggle-on"); } else { greetingOutput40.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv41 = document.getElementById("ratingControlDiv41");// Retrieve the div that hosts the Toggle control.
            var toggleControl41 = toggleControlDiv41.winControl;// Retrieve the actual Toggle control.
            toggleControl41.addEventListener("change", toggleChanged41); // Register the event handler. 
            var roamingSettings41 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection41 = roamingSettings41.values["toggleSelection41"];// Restore the Toggle selection.
            if (toggleSelection41) {
                toggleControl41.checked = toggleSelection41;
                var greetingOutput41 = document.getElementById("41");// Apply the properties of the toggle selection
                if (toggleControl41.checked == true) { greetingOutput41.setAttribute("class", "toggle-on"); } else { greetingOutput41.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv42 = document.getElementById("ratingControlDiv42");// Retrieve the div that hosts the Toggle control.
            var toggleControl42 = toggleControlDiv42.winControl;// Retrieve the actual Toggle control.
            toggleControl42.addEventListener("change", toggleChanged42); // Register the event handler. 
            var roamingSettings42 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection42 = roamingSettings42.values["toggleSelection42"];// Restore the Toggle selection.
            if (toggleSelection42) {
                toggleControl42.checked = toggleSelection42;
                var greetingOutput42 = document.getElementById("42");// Apply the properties of the toggle selection
                if (toggleControl42.checked == true) { greetingOutput42.setAttribute("class", "toggle-on"); } else { greetingOutput42.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv43 = document.getElementById("ratingControlDiv43");// Retrieve the div that hosts the Toggle control.
            var toggleControl43 = toggleControlDiv43.winControl;// Retrieve the actual Toggle control.
            toggleControl43.addEventListener("change", toggleChanged43); // Register the event handler. 
            var roamingSettings43 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection43 = roamingSettings43.values["toggleSelection43"];// Restore the Toggle selection.
            if (toggleSelection43) {
                toggleControl43.checked = toggleSelection43;
                var greetingOutput43 = document.getElementById("43");// Apply the properties of the toggle selection
                if (toggleControl43.checked == true) { greetingOutput43.setAttribute("class", "toggle-on"); } else { greetingOutput43.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv44 = document.getElementById("ratingControlDiv44");// Retrieve the div that hosts the Toggle control.
            var toggleControl44 = toggleControlDiv44.winControl;// Retrieve the actual Toggle control.
            toggleControl44.addEventListener("change", toggleChanged44); // Register the event handler. 
            var roamingSettings44 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection44 = roamingSettings44.values["toggleSelection44"];// Restore the Toggle selection.
            if (toggleSelection44) {
                toggleControl44.checked = toggleSelection44;
                var greetingOutput44 = document.getElementById("44");// Apply the properties of the toggle selection
                if (toggleControl44.checked == true) { greetingOutput44.setAttribute("class", "toggle-on"); } else { greetingOutput44.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv45 = document.getElementById("ratingControlDiv45");// Retrieve the div that hosts the Toggle control.
            var toggleControl45 = toggleControlDiv45.winControl;// Retrieve the actual Toggle control.
            toggleControl45.addEventListener("change", toggleChanged45); // Register the event handler. 
            var roamingSettings45 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection45 = roamingSettings45.values["toggleSelection45"];// Restore the Toggle selection.
            if (toggleSelection45) {
                toggleControl45.checked = toggleSelection45;
                var greetingOutput45 = document.getElementById("45");// Apply the properties of the toggle selection
                if (toggleControl45.checked == true) { greetingOutput45.setAttribute("class", "toggle-on"); } else { greetingOutput45.removeAttribute("class", "toggle-on"); }
            }
            ///end.
            ///begin
            var toggleControlDiv46 = document.getElementById("ratingControlDiv46");// Retrieve the div that hosts the Toggle control.
            var toggleControl46 = toggleControlDiv46.winControl;// Retrieve the actual Toggle control.
            toggleControl46.addEventListener("change", toggleChanged46); // Register the event handler. 
            var roamingSettings46 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection46 = roamingSettings46.values["toggleSelection46"];// Restore the Toggle selection.
            if (toggleSelection46) {
                toggleControl46.checked = toggleSelection46;
                var greetingOutput46 = document.getElementById("46");// Apply the properties of the toggle selection
                if (toggleControl46.checked == true) { greetingOutput46.setAttribute("class", "toggle-on"); } else { greetingOutput46.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv48 = document.getElementById("ratingControlDiv48");// Retrieve the div that hosts the Toggle control.
            var toggleControl48 = toggleControlDiv48.winControl;// Retrieve the actual Toggle control.
            toggleControl48.addEventListener("change", toggleChanged48); // Register the event handler. 
            var roamingSettings48 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection48 = roamingSettings48.values["toggleSelection48"];// Restore the Toggle selection.
            if (toggleSelection48) {
                toggleControl48.checked = toggleSelection48;
                var greetingOutput48 = document.getElementById("48");// Apply the properties of the toggle selection
                if (toggleControl48.checked == true) { greetingOutput48.setAttribute("class", "toggle-on"); } else { greetingOutput48.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv47 = document.getElementById("ratingControlDiv47");// Retrieve the div that hosts the Toggle control.
            var toggleControl47 = toggleControlDiv47.winControl;// Retrieve the actual Toggle control.
            toggleControl47.addEventListener("change", toggleChanged47); // Register the event handler. 
            var roamingSettings47 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection47 = roamingSettings47.values["toggleSelection47"];// Restore the Toggle selection.
            if (toggleSelection47) {
                toggleControl47.checked = toggleSelection47;
                var greetingOutput47 = document.getElementById("47");// Apply the properties of the toggle selection
                if (toggleControl47.checked == true) { greetingOutput47.setAttribute("class", "toggle-on"); } else { greetingOutput47.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv49 = document.getElementById("ratingControlDiv49");// Retrieve the div that hosts the Toggle control.
            var toggleControl49 = toggleControlDiv49.winControl;// Retrieve the actual Toggle control.
            toggleControl49.addEventListener("change", toggleChanged49); // Register the event handler. 
            var roamingSettings49 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection49 = roamingSettings49.values["toggleSelection49"];// Restore the Toggle selection.
            if (toggleSelection49) {
                toggleControl49.checked = toggleSelection49;
                var greetingOutput49 = document.getElementById("49");// Apply the properties of the toggle selection
                if (toggleControl49.checked == true) { greetingOutput49.setAttribute("class", "toggle-on"); } else { greetingOutput49.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv50 = document.getElementById("ratingControlDiv50");// Retrieve the div that hosts the Toggle control.
            var toggleControl50 = toggleControlDiv50.winControl;// Retrieve the actual Toggle control.
            toggleControl50.addEventListener("change", toggleChanged50); // Register the event handler. 
            var roamingSettings50 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection50 = roamingSettings50.values["toggleSelection50"];// Restore the Toggle selection.
            if (toggleSelection50) {
                toggleControl50.checked = toggleSelection50;
                var greetingOutput50 = document.getElementById("50");// Apply the properties of the toggle selection
                if (toggleControl50.checked == true) { greetingOutput50.setAttribute("class", "toggle-on"); } else { greetingOutput50.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv51 = document.getElementById("ratingControlDiv51");// Retrieve the div that hosts the Toggle control.
            var toggleControl51 = toggleControlDiv51.winControl;// Retrieve the actual Toggle control.
            toggleControl51.addEventListener("change", toggleChanged51); // Register the event handler. 
            var roamingSettings51 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection51 = roamingSettings51.values["toggleSelection51"];// Restore the Toggle selection.
            if (toggleSelection51) {
                toggleControl51.checked = toggleSelection51;
                var greetingOutput51 = document.getElementById("51");// Apply the properties of the toggle selection
                if (toggleControl51.checked == true) { greetingOutput51.setAttribute("class", "toggle-on"); } else { greetingOutput51.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv52 = document.getElementById("ratingControlDiv52");// Retrieve the div that hosts the Toggle control.
            var toggleControl52 = toggleControlDiv52.winControl;// Retrieve the actual Toggle control.
            toggleControl52.addEventListener("change", toggleChanged52); // Register the event handler. 
            var roamingSettings52 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection52 = roamingSettings52.values["toggleSelection52"];// Restore the Toggle selection.
            if (toggleSelection52) {
                toggleControl52.checked = toggleSelection52;
                var greetingOutput52 = document.getElementById("52");// Apply the properties of the toggle selection
                if (toggleControl52.checked == true) { greetingOutput52.setAttribute("class", "toggle-on"); } else { greetingOutput52.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv53 = document.getElementById("ratingControlDiv53");// Retrieve the div that hosts the Toggle control.
            var toggleControl53 = toggleControlDiv53.winControl;// Retrieve the actual Toggle control.
            toggleControl53.addEventListener("change", toggleChanged53); // Register the event handler. 
            var roamingSettings53 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection53 = roamingSettings53.values["toggleSelection53"];// Restore the Toggle selection.
            if (toggleSelection53) {
                toggleControl53.checked = toggleSelection53;
                var greetingOutput53 = document.getElementById("53");// Apply the properties of the toggle selection
                if (toggleControl53.checked == true) { greetingOutput53.setAttribute("class", "toggle-on"); } else { greetingOutput53.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv54 = document.getElementById("ratingControlDiv54");// Retrieve the div that hosts the Toggle control.
            var toggleControl54 = toggleControlDiv54.winControl;// Retrieve the actual Toggle control.
            toggleControl54.addEventListener("change", toggleChanged54); // Register the event handler. 
            var roamingSettings54 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection54 = roamingSettings54.values["toggleSelection54"];// Restore the Toggle selection.
            if (toggleSelection54) {
                toggleControl54.checked = toggleSelection54;
                var greetingOutput54 = document.getElementById("54");// Apply the properties of the toggle selection
                if (toggleControl54.checked == true) { greetingOutput54.setAttribute("class", "toggle-on"); } else { greetingOutput54.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv55 = document.getElementById("ratingControlDiv55");// Retrieve the div that hosts the Toggle control.
            var toggleControl55 = toggleControlDiv55.winControl;// Retrieve the actual Toggle control.
            toggleControl55.addEventListener("change", toggleChanged55); // Register the event handler. 
            var roamingSettings55 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection55 = roamingSettings55.values["toggleSelection55"];// Restore the Toggle selection.
            if (toggleSelection55) {
                toggleControl55.checked = toggleSelection55;
                var greetingOutput55 = document.getElementById("55");// Apply the properties of the toggle selection
                if (toggleControl55.checked == true) { greetingOutput55.setAttribute("class", "toggle-on"); } else { greetingOutput55.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv56 = document.getElementById("ratingControlDiv56");// Retrieve the div that hosts the Toggle control.
            var toggleControl56 = toggleControlDiv56.winControl;// Retrieve the actual Toggle control.
            toggleControl56.addEventListener("change", toggleChanged56); // Register the event handler. 
            var roamingSettings56 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection56 = roamingSettings56.values["toggleSelection56"];// Restore the Toggle selection.
            if (toggleSelection56) {
                toggleControl56.checked = toggleSelection56;
                var greetingOutput56 = document.getElementById("56");// Apply the properties of the toggle selection
                if (toggleControl56.checked == true) { greetingOutput56.setAttribute("class", "toggle-on"); } else { greetingOutput56.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv57 = document.getElementById("ratingControlDiv57");// Retrieve the div that hosts the Toggle control.
            var toggleControl57 = toggleControlDiv57.winControl;// Retrieve the actual Toggle control.
            toggleControl57.addEventListener("change", toggleChanged57); // Register the event handler. 
            var roamingSettings57 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection57 = roamingSettings57.values["toggleSelection57"];// Restore the Toggle selection.
            if (toggleSelection57) {
                toggleControl57.checked = toggleSelection57;
                var greetingOutput57 = document.getElementById("57");// Apply the properties of the toggle selection
                if (toggleControl57.checked == true) { greetingOutput57.setAttribute("class", "toggle-on"); } else { greetingOutput57.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv58 = document.getElementById("ratingControlDiv58");// Retrieve the div that hosts the Toggle control.
            var toggleControl58 = toggleControlDiv58.winControl;// Retrieve the actual Toggle control.
            toggleControl58.addEventListener("change", toggleChanged58); // Register the event handler. 
            var roamingSettings58 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection58 = roamingSettings58.values["toggleSelection58"];// Restore the Toggle selection.
            if (toggleSelection58) {
                toggleControl58.checked = toggleSelection58;
                var greetingOutput58 = document.getElementById("58");// Apply the properties of the toggle selection
                if (toggleControl58.checked == true) { greetingOutput58.setAttribute("class", "toggle-on"); } else { greetingOutput58.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv59 = document.getElementById("ratingControlDiv59");// Retrieve the div that hosts the Toggle control.
            var toggleControl59 = toggleControlDiv59.winControl;// Retrieve the actual Toggle control.
            toggleControl59.addEventListener("change", toggleChanged59); // Register the event handler. 
            var roamingSettings59 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection59 = roamingSettings59.values["toggleSelection59"];// Restore the Toggle selection.
            if (toggleSelection59) {
                toggleControl59.checked = toggleSelection59;
                var greetingOutput59 = document.getElementById("59");// Apply the properties of the toggle selection
                if (toggleControl59.checked == true) { greetingOutput59.setAttribute("class", "toggle-on"); } else { greetingOutput59.removeAttribute("class", "toggle-on"); }
            }
            ///end
            ///begin
            var toggleControlDiv60 = document.getElementById("ratingControlDiv60");// Retrieve the div that hosts the Toggle control.
            var toggleControl60 = toggleControlDiv60.winControl;// Retrieve the actual Toggle control.
            toggleControl60.addEventListener("change", toggleChanged60); // Register the event handler. 
            var roamingSettings60 = Windows.Storage.ApplicationData.current.roamingSettings;// Restore app data.
            var toggleSelection60 = roamingSettings60.values["toggleSelection60"];// Restore the Toggle selection.
            if (toggleSelection60) {
                toggleControl60.checked = toggleSelection60;
                var greetingOutput60 = document.getElementById("60");// Apply the properties of the toggle selection
                if (toggleControl60.checked == true) { greetingOutput60.setAttribute("class", "toggle-on"); } else { greetingOutput60.removeAttribute("class", "toggle-on"); }
            }
            ///end

        }
    });


	function toggleChanged1(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv1").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("1");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection1"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged2(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv2").winControl;
	    var greetingOutput = document.getElementById("2");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection2"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged3(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv3").winControl;
	    var greetingOutput = document.getElementById("3");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection3"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged4(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv4").winControl;
	    var greetingOutput = document.getElementById("4");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection4"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged5(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv5").winControl;
	    var greetingOutput = document.getElementById("5");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection5"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged6(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv6").winControl;
	    var greetingOutput = document.getElementById("6");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection6"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged7(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv7").winControl;
	    var greetingOutput = document.getElementById("7");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection7"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged8(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv8").winControl;
	    var greetingOutput = document.getElementById("8");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection8"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged9(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv9").winControl;
	    var greetingOutput = document.getElementById("9");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection9"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged10(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv10").winControl;
	    var greetingOutput = document.getElementById("10");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection10"] = toggleControl.checked;
	}
    //end
	function toggleChanged1(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv1").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("1");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection1"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged2(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv2").winControl;
	    var greetingOutput = document.getElementById("2");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection2"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged3(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv3").winControl;
	    var greetingOutput = document.getElementById("3");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection3"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged4(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv4").winControl;
	    var greetingOutput = document.getElementById("4");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection4"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged5(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv5").winControl;
	    var greetingOutput = document.getElementById("5");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection5"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged6(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv6").winControl;
	    var greetingOutput = document.getElementById("6");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection6"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged7(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv7").winControl;
	    var greetingOutput = document.getElementById("7");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection7"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged8(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv8").winControl;
	    var greetingOutput = document.getElementById("8");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection8"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged9(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv9").winControl;
	    var greetingOutput = document.getElementById("9");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection9"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged10(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv10").winControl;
	    var greetingOutput = document.getElementById("10");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection10"] = toggleControl.checked;
	}
    //end
	function toggleChanged11(eventInfo) {
	    // Get the toggle c1ontrol
	    var toggleControl = document.getElementById("ratingControlDiv11").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("11");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection11"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged12(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv12").winControl;
	    var greetingOutput = document.getElementById("12");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection12"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged13(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv13").winControl;
	    var greetingOutput = document.getElementById("13");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection13"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged14(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv14").winControl;
	    var greetingOutput = document.getElementById("14");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection14"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged15(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv15").winControl;
	    var greetingOutput = document.getElementById("15");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection15"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged16(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv16").winControl;
	    var greetingOutput = document.getElementById("16");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection16"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged17(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv17").winControl;
	    var greetingOutput = document.getElementById("17");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection17"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged18(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv18").winControl;
	    var greetingOutput = document.getElementById("18");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection18"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged19(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv19").winControl;
	    var greetingOutput = document.getElementById("19");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection19"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged20(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv20").winControl;
	    var greetingOutput = document.getElementById("20");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection20"] = toggleControl.checked;
	}
    //end
	function toggleChanged21(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv21").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("21");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection21"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged22(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv22").winControl;
	    var greetingOutput = document.getElementById("22");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection22"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged23(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv23").winControl;
	    var greetingOutput = document.getElementById("23");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection23"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged24(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv24").winControl;
	    var greetingOutput = document.getElementById("24");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection24"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged25(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv25").winControl;
	    var greetingOutput = document.getElementById("25");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection25"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged26(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv26").winControl;
	    var greetingOutput = document.getElementById("26");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection26"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged27(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv27").winControl;
	    var greetingOutput = document.getElementById("27");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection27"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged28(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv28").winControl;
	    var greetingOutput = document.getElementById("28");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection28"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged29(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv29").winControl;
	    var greetingOutput = document.getElementById("29");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection29"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged30(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv30").winControl;
	    var greetingOutput = document.getElementById("30");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection30"] = toggleControl.checked;
	}
    //end
	function toggleChanged31(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv31").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("31");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection31"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged32(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv32").winControl;
	    var greetingOutput = document.getElementById("32");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection32"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged33(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv33").winControl;
	    var greetingOutput = document.getElementById("33");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection33"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged34(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv34").winControl;
	    var greetingOutput = document.getElementById("34");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection34"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged35(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv35").winControl;
	    var greetingOutput = document.getElementById("35");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection35"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged36(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv36").winControl;
	    var greetingOutput = document.getElementById("36");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection36"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged37(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv37").winControl;
	    var greetingOutput = document.getElementById("37");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection37"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged38(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv38").winControl;
	    var greetingOutput = document.getElementById("38");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection38"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged39(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv39").winControl;
	    var greetingOutput = document.getElementById("39");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection39"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged40(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv40").winControl;
	    var greetingOutput = document.getElementById("40");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection40"] = toggleControl.checked;
	}
    //end
	function toggleChanged41(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv41").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("41");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection41"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged42(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv42").winControl;
	    var greetingOutput = document.getElementById("42");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection42"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged43(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv43").winControl;
	    var greetingOutput = document.getElementById("43");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection43"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged44(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv44").winControl;
	    var greetingOutput = document.getElementById("44");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection44"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged45(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv45").winControl;
	    var greetingOutput = document.getElementById("45");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection45"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged46(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv46").winControl;
	    var greetingOutput = document.getElementById("46");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection46"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged47(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv47").winControl;
	    var greetingOutput = document.getElementById("47");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection47"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged48(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv48").winControl;
	    var greetingOutput = document.getElementById("48");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection48"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged49(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv49").winControl;
	    var greetingOutput = document.getElementById("49");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection49"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged50(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv50").winControl;
	    var greetingOutput = document.getElementById("50");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection50"] = toggleControl.checked;
	}
    //end
	function toggleChanged51(eventInfo) {
	    // Get the toggle control
	    var toggleControl = document.getElementById("ratingControlDiv51").winControl;

	    // Get the greeting output
	    var greetingOutput = document.getElementById("51");

	    // Set the CSS class for the greeting output based on the toggle's state
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }

	    // Store the toggle selection for multiple sessions.
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection51"] = toggleControl.checked;
	}
    ///begin
	function toggleChanged52(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv52").winControl;
	    var greetingOutput = document.getElementById("52");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection52"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged53(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv53").winControl;
	    var greetingOutput = document.getElementById("53");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection53"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged54(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv54").winControl;
	    var greetingOutput = document.getElementById("54");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection54"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged55(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv55").winControl;
	    var greetingOutput = document.getElementById("55");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection55"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged56(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv56").winControl;
	    var greetingOutput = document.getElementById("56");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection56"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged57(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv57").winControl;
	    var greetingOutput = document.getElementById("57");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection57"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged58(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv58").winControl;
	    var greetingOutput = document.getElementById("58");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection58"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged59(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv59").winControl;
	    var greetingOutput = document.getElementById("59");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection59"] = toggleControl.checked;
	}
    //end
    ///begin
	function toggleChanged60(eventInfo) {
	    var toggleControl = document.getElementById("ratingControlDiv60").winControl;
	    var greetingOutput = document.getElementById("60");
	    if (toggleControl.checked == true) {
	        greetingOutput.setAttribute("class", "toggle-on");
	    }
	    else {
	        greetingOutput.removeAttribute("class", "toggle-on");
	    }
	    var appData = Windows.Storage.ApplicationData.current;
	    var roamingSettings = appData.roamingSettings;
	    roamingSettings.values["toggleSelection60"] = toggleControl.checked;
	}
    //end
})();

