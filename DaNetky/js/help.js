﻿// Основные сведения о пустом шаблоне см. в следующей документации:
// http://go.microsoft.com/fwlink/?LinkId=232509
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    var local = WinJS.Application.local;

    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }

    // Установить обработчики клавиатуры
    function nocopy(event) {
        var event = event || window.event;
        if (event.preventDefault) { event.preventDefault(); }
        else { event.returnValue = false; }
        return false;
    }

	function goToPageGameHome() {
	    document.location.href = "/default.html";
	}
	var page = WinJS.UI.Pages.define("help.html", {
        ready: function (element, options) {
            backHome.onclick = function (evt) { goToPageGameHome(); }

            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);

            document.onmousedown = nocopy;
            document.onmouseup = nocopy;
            document.onmousemove = nocopy;
            document.ondragstart = nocopy;
            document.onselectstart = nocopy;
            document.ontextmenu = nocopy;
            document.oncopy = nocopy;


        }
    });


})();

